package com.quangnd.mvpdemo.model

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class User(): Parcelable {

    var id: Int = 0
    var email: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var avatar: String = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        email = parcel.readString() ?: ""
        firstName = parcel.readString() ?: ""
        lastName = parcel.readString() ?: ""
        avatar = parcel.readString() ?: ""
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(email)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(avatar)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {

        fun parseData(source: JSONObject): User {
            val result = User()
            result.let {
                it.id = source.optInt("id")
                it.email = source.optString("email")
                it.firstName = source.optString("first_name")
                it.lastName = source.optString("last_name")
                it.avatar = source.optString("avatar")
            }
            return result
        }


        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

}