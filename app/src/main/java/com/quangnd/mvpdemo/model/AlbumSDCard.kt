package com.quangnd.mvpdemo.model

import android.os.Parcel
import android.os.Parcelable

class AlbumSDCard() : Parcelable {
    var name: String = ""
    var path: String = ""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString().toString()
        path = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(path)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AlbumSDCard> {
        override fun createFromParcel(parcel: Parcel): AlbumSDCard {
            return AlbumSDCard(parcel)
        }

        override fun newArray(size: Int): Array<AlbumSDCard?> {
            return arrayOfNulls(size)
        }
    }

}