package com.quangnd.mvpdemo.data.api

import com.google.gson.JsonObject
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.http.*

interface RequestInterface {

    @POST("/api/login")
    @FormUrlEncoded
    fun login(@Field("email") email: String,
              @Field("password") password: String)
            : Observable<JSONObject>

    @GET("/api/users")
    fun getListUser(@Query(value = "page") page: Int)
            : Observable<JsonObject>
}