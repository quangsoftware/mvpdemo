package com.quangnd.mvpdemo.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.quangnd.mvpdemo.R
import kotlinx.android.synthetic.main.dialog_alert.view.*
import kotlinx.android.synthetic.main.dialog_alert.view.tvLeft
import kotlinx.android.synthetic.main.dialog_alert.view.tvRight
import kotlinx.android.synthetic.main.dialog_alert.view.tvTitleDialog
import kotlinx.android.synthetic.main.item_photo.*
import kotlinx.android.synthetic.main.photo_dialog_alert.view.*

object AppUtils {
    fun createAlertDialog(
            context: Context,
            title: String,
            content: String,
            keep: String,
            discard: String,
            action: ActionAlert
    ): AlertDialog {
        val win = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val width = win.defaultDisplay.width
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_alert, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
        mDialogView.tvLeft.text = keep
        mDialogView.tvRight.text = discard
        mDialogView.tvTitleDialog.text = title
        mDialogView.tvContent.text = content
        mDialogView.tvLeft.setOnClickListener {
            action.onKeep()

        }
        //cancel button click of custom layout
        mDialogView.tvRight.setOnClickListener {
            action.onDiscard()

        }
        val alert = mBuilder.show()
        alert?.window?.setLayout((width * 0.7).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        alert?.window?.setBackgroundDrawableResource(R.color.color_00000000)

        return alert
    }

    fun createPhotoAlertDialog(
        context: Context,
        title: String,
        pathImage: String,
        keep: String,
        discard: String,
        action: ActionAlert
    ): AlertDialog {
        val win = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val width = win.defaultDisplay.width
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.photo_dialog_alert, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        mDialogView.tvLeft.text = keep
        mDialogView.tvRight.text = discard
        mDialogView.tvTitleDialog.text = title
        Glide.with(context)
            .load(pathImage)
            .into(mDialogView.imageView)
        mDialogView.tvLeft.setOnClickListener {
            action.onKeep()

        }
        //cancel button click of custom layout
        mDialogView.tvRight.setOnClickListener {
            action.onDiscard()

        }
        val alert = mBuilder.show()
        alert?.window?.setLayout((width * 0.7).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        alert?.window?.setBackgroundDrawableResource(R.color.color_00000000)

        return alert
    }

    interface ActionAlert {
        fun onKeep()
        fun onDiscard()
    }
}