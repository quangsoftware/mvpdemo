package com.quangnd.mvpdemo.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.view.adapter.AlphabetPagerAdapter
import kotlinx.android.synthetic.main.activity_view_paper.*

class ViewPaperActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_paper)

        // Initialize a list of string values
        val alphabets = listOf("a","b","c","d","e","f")

        // Initialize a new pager adapter instance with list
        val adapter = AlphabetPagerAdapter(alphabets)

        // Finally, data bind the view pager widget with pager adapter
        view_pager.adapter = adapter

        indicator.setViewPager(view_pager);
    }
}