package com.quangnd.mvpdemo.view.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.User
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_photo.*

class PhotoAdapter(val images: List<String>):RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    var mContext: Context? = null
    private var mAction: PhotoAdapter.OnPhotoAction? = null

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: PhotoAdapter.ViewHolder, position: Int) {
        holder.bindData(holder.adapterPosition, images[holder.adapterPosition])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoAdapter.ViewHolder {
        mContext = parent.context
        return ViewHolder(View.inflate(mContext, R.layout.item_photo, null))
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindData(position: Int, pathImage: String) {
            if (position == 0) {
                ivPhoto.setImageResource(R.mipmap.no_image)
            } else {
                mContext?.let {
                    Glide.with(it)
                        .load(pathImage)
                        .into(ivPhoto)
                }
            }

            containerView!!.setOnClickListener {
                mAction?.onClickItem(pathImage, position)
            }
        }
    }

    fun setListener(action: OnPhotoAction) {
        mAction = action
    }

    interface OnPhotoAction {
        fun onClickItem(pathImage: String, position: Int)
    }

}