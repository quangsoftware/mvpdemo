package com.quangnd.mvpdemo.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.User
import com.quangnd.mvpdemo.presenter.UserPresenterImpl
import com.quangnd.mvpdemo.presenter.UserView
import com.quangnd.mvpdemo.view.adapter.StaggerViewAdapter
import kotlinx.android.synthetic.main.activity_staggeredlayout.*

class StaggeredlayoutActivity : UserView, AppCompatActivity() {

    lateinit var userPresenterImpl: UserPresenterImpl
    var mUserList: ArrayList<User?> = ArrayList()
    var page: Int = 1
    var isLoading: Boolean = false;
    lateinit var mStaggerViewAdapter: StaggerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staggeredlayout)

        userPresenterImpl = UserPresenterImpl(this, this@StaggeredlayoutActivity)

        mStaggerViewAdapter = StaggerViewAdapter((mUserList))

        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        rcvStaggerLayout.layoutManager = staggeredGridLayoutManager
        rcvStaggerLayout.setHasFixedSize(true)
        rcvStaggerLayout.adapter = mStaggerViewAdapter

        rcvStaggerLayout.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val staggeredGridLayoutManager = recyclerView.layoutManager as StaggeredGridLayoutManager;
                if (!isLoading) {
                    val lastVisibleItemPositions = staggeredGridLayoutManager.findLastVisibleItemPositions(null)
                    val lastVisibleItem = getLastVisibleItem(lastVisibleItemPositions)
                    if (lastVisibleItem + staggeredGridLayoutManager.spanCount*2 >= mUserList.size) {
                        loadMore();
                    }
                }
            }
        })

        loadMore()
    }

    fun loadMore() {
        isLoading = true;
        mUserList.add(null);
        mStaggerViewAdapter?.notifyItemInserted(mUserList.size - 1)
        Handler().postDelayed({
            userPresenterImpl.getListUser(page)
        }, 5000)

    }

    override fun getListUserFail(error: String) {
        hideLoadMore()
        Snackbar.make(rcvStaggerLayout, error, Snackbar.LENGTH_LONG).show()
    }

    override fun getListUserSuccess(userList: List<User>, page: Int) {
        if (userList.isEmpty()) {
            hideLoadMore()
            Snackbar.make(rcvStaggerLayout, "Data Empty", Snackbar.LENGTH_LONG).show()
        } else {
            this.page = page

            hideLoadMore()

            mUserList.addAll(userList)

            mStaggerViewAdapter.notifyDataSetChanged()
            isLoading = false
        }
    }

    fun hideLoadMore() {
        mUserList.removeAt(mUserList.size - 1)
        val scrollPosition = mUserList.size
        mStaggerViewAdapter.notifyItemRemoved(scrollPosition)
    }

    private fun getLastVisibleItem(lastVisibleItemPositions: IntArray): Int {
        var maxSize = 0
        for (i in lastVisibleItemPositions.indices) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i]
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i]
            }
        }
        return maxSize
    }
}