package com.quangnd.mvpdemo.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.User
import com.quangnd.mvpdemo.utils.Constant
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.gridview_item.*

class StaggerViewAdapter(val userList: List<User?>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mContext: Context? = null

    companion object{
        val VIEW_TYPE_ITEM: Int = 0;
        val VIEW_TYPE_LOADING: Int = 1;
    }


    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> userList[holder.adapterPosition]?.let { holder.bindData(holder.adapterPosition, it) }
            is LoadingViewHolder -> holder.bindData()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        return when(viewType){
            VIEW_TYPE_ITEM -> ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.stagger_item_row, parent, false))
            VIEW_TYPE_LOADING -> LoadingViewHolder(LayoutInflater.from(mContext).inflate(R.layout.progress_loading, parent, false))
            else -> LoadingViewHolder(View.inflate(mContext, R.layout.progress_loading, parent))
        }
        return ViewHolder(View.inflate(mContext, R.layout.gridview_item, null))
    }

    override fun getItemViewType(position: Int): Int {
        return if (userList[position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindData(position: Int, user: User) {
            mContext?.let {
                Glide.with(it)
                    .load(user.avatar)
                    .apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.no_image))
                    .into(ivAvatar)
            }
            val rnds = (1..10).random()
            var txt = ""
            for (i in 1..rnds) {
                txt += "${user.firstName} ${user.lastName} "
            }
            tvName.text = txt

            containerView!!.setOnClickListener {
//                mAction?.onClickItem(user, position)
            }
        }
    }

    inner class LoadingViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindData() {
            val layoutParams = containerView.layoutParams as StaggeredGridLayoutManager.LayoutParams
            layoutParams.isFullSpan = true
        }
    }
}