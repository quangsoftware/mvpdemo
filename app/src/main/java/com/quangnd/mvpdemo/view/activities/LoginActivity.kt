package com.quangnd.mvpdemo.view.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.presenter.LoginPresenterImpl
import com.quangnd.mvpdemo.presenter.LoginView
import com.quangnd.mvpdemo.utils.AppUtils
import kotlinx.android.synthetic.main.activity_login.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class LoginActivity : LoginView, AppCompatActivity() {

    var loginPresenterImpl: LoginPresenterImpl? =null
    lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !==
                PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            }
        }

        loginPresenterImpl = LoginPresenterImpl(this, this@LoginActivity)

        etEmail.setText("eve.holt@reqres.in")
        etPassword.setText("cityslicka")

        btnLogin.setOnClickListener {
            val email = etEmail.text.toString()
            val pwd = etPassword.text.toString()

            dialog = AppUtils.createAlertDialog(this, "Notification", "You are want to login?","Yes", "No",
                    object : AppUtils.ActionAlert {
                override fun onKeep() {
                    loginPresenterImpl?.login(email, pwd)
                    dialog?.dismiss()
                }

                override fun onDiscard() {
                    dialog?.dismiss()
                }

            })
        }

        tvSkip.setOnClickListener {
            val intent = Intent(this, PhotoActivity::class.java)
            startActivity(intent)
        }

        ivFacebook.setOnClickListener {
            val intent = Intent(this, StaggeredlayoutActivity::class.java)
            startActivity(intent)
        }

        btnMap.setOnClickListener {
            val intent = Intent(this, MapActivity::class.java)
            startActivity(intent)
        }

    }

    override fun loginSuccess() {
        Snackbar.make(etEmail, "Login Success", Snackbar.LENGTH_SHORT).show();

        val intent = Intent(this, UserActivity::class.java)
        startActivity(intent)
    }

    override fun loginFail(error: String) {
        Snackbar.make(etEmail, error, Snackbar.LENGTH_SHORT).show();
    }

    override fun validateFail(error: String) {
        Snackbar.make(etEmail, error, Snackbar.LENGTH_SHORT).show();
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE) ===
                                    PackageManager.PERMISSION_GRANTED)) {
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }
}