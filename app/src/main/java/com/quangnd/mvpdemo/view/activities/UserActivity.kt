package com.quangnd.mvpdemo.view.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.User
import com.quangnd.mvpdemo.presenter.UserPresenterImpl
import com.quangnd.mvpdemo.presenter.UserView
import com.quangnd.mvpdemo.view.adapter.UserAdapter
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : UserView, AppCompatActivity() {

    lateinit var userPresenterImpl: UserPresenterImpl
    var mUserList: ArrayList<User?> = ArrayList()
    lateinit var mUserAdapter: UserAdapter
    var page: Int = 1
    var isLoading: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        val actionbar = supportActionBar
        //set back button
        actionbar?.setDisplayHomeAsUpEnabled(true)

        userPresenterImpl = UserPresenterImpl(this, this@UserActivity)

        mUserAdapter = UserAdapter(mUserList)
        rcvUser.adapter = mUserAdapter
        rcvUser.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mUserAdapter
        }

        rcvUser.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager;
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mUserList.size - 1) {
                        loadMore();
                    }
                }
            }
        })

        mUserAdapter!!.setListener(object : UserAdapter.OnUserAction {
            override fun onClickItem(user: User, position: Int) {
                val intent = Intent(this@UserActivity, ViewPaperActivity::class.java)
                startActivity(intent)
            }
        })

        loadMore();
    }

    fun loadMore() {
        isLoading = true;
        mUserList.add(null);
        mUserAdapter?.notifyItemInserted(mUserList.size - 1)
        userPresenterImpl!!.getListUser(page)
    }

    override fun getListUserFail(error: String) {
        hideLoadMore()
        Snackbar.make(rcvUser, error, Snackbar.LENGTH_LONG).show()
    }

    override fun getListUserSuccess(userList: List<User>, page: Int) {
        if (userList.isEmpty()) {
            hideLoadMore()
            Snackbar.make(rcvUser, "Data Empty", Snackbar.LENGTH_LONG).show()
        } else {
            this.page = page

            hideLoadMore()

            mUserList.addAll(userList)

            rcvUser.post {
                mUserAdapter?.notifyDataSetChanged()
            }
            isLoading = false
        }
    }

    fun hideLoadMore() {
        mUserList.removeAt(mUserList.size - 1)
        val scrollPosition = mUserList.size
        mUserAdapter.notifyItemRemoved(scrollPosition)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
