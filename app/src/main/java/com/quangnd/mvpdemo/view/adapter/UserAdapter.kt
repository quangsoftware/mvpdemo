package com.quangnd.mvpdemo.view.adapter

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.User
import com.quangnd.mvpdemo.utils.Constant
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_user.*

class UserAdapter(private var userList: List<User?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mContext: Context? = null
    private var mAction: OnUserAction? = null

    val VIEW_TYPE_ITEM: Int = 0;
    val VIEW_TYPE_LOADING: Int = 1;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        return when(viewType){
            VIEW_TYPE_ITEM -> ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_user, parent, false))
            VIEW_TYPE_LOADING -> LoadingViewHolder(LayoutInflater.from(mContext).inflate(R.layout.progress_loading, parent, false))
            else -> LoadingViewHolder(View.inflate(mContext, R.layout.progress_loading, parent))
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> userList[holder.adapterPosition]?.let { holder.bindData(holder.adapterPosition, it) }
            is LoadingViewHolder -> holder.bindData()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (userList[position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindData(position: Int, user: User) {
            mContext?.let {
                Glide.with(it)
                    .load(user.avatar)
                    .apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.no_image))
                    .into(ivAvatar)
            }
            tvName.text = "${user.firstName} ${user.lastName}"
            tvEmail.text = user.email

            containerView!!.setOnClickListener {
                mAction?.onClickItem(user, position)
            }
        }
    }


    inner class LoadingViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindData() {
        }
    }

    fun setListener(action: OnUserAction) {
        mAction = action
    }

    interface OnUserAction {
        fun onClickItem(user: User, position: Int)
    }


}