package com.quangnd.mvpdemo.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.User
import com.quangnd.mvpdemo.presenter.UserPresenterImpl
import com.quangnd.mvpdemo.presenter.UserView
import com.quangnd.mvpdemo.utils.Constant.VIEW_TYPE_LOADING
import com.quangnd.mvpdemo.view.adapter.GridViewAdapter
import kotlinx.android.synthetic.main.activity_grid_view.*
import kotlinx.android.synthetic.main.activity_grid_view.rcvUser
import kotlinx.android.synthetic.main.activity_user.*

class GridViewActivity : UserView, AppCompatActivity() {

    lateinit var userPresenterImpl: UserPresenterImpl
    var mUserList: ArrayList<User?> = ArrayList()
    lateinit var mGridViewAdapter: GridViewAdapter
    var page: Int = 1
    var isLoading: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_view)

        userPresenterImpl = UserPresenterImpl(this, this@GridViewActivity)

        mGridViewAdapter = GridViewAdapter(mUserList)

        val mLayoutManager = GridLayoutManager(this, 2)
        rcvUser.layoutManager = mLayoutManager
        rcvUser.setHasFixedSize(true)
        rcvUser.adapter = mGridViewAdapter
        (mLayoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (mGridViewAdapter.getItemViewType(position)) {
                    GridViewAdapter.VIEW_TYPE_ITEM -> 1
                    GridViewAdapter.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                    else -> -1
                }
            }
        }

        rcvUser.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager;
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mUserList.size - 1) {
                        loadMore();
                    }
                }
            }
        })

        loadMore()
    }

    fun loadMore() {
        isLoading = true;
        mUserList.add(null);
        mGridViewAdapter?.notifyItemInserted(mUserList.size - 1)
        userPresenterImpl!!.getListUser(page)
    }

    override fun getListUserFail(error: String) {
        hideLoadMore()
        Snackbar.make(rcvUser, error, Snackbar.LENGTH_LONG).show()
    }

    override fun getListUserSuccess(userList: List<User>, page: Int) {
        if (userList.isEmpty()) {
            hideLoadMore()
            Snackbar.make(rcvUser, "Data Empty", Snackbar.LENGTH_LONG).show()
        } else {
            this.page = page

            hideLoadMore()

            mUserList.addAll(userList)

            mGridViewAdapter?.notifyDataSetChanged()
            isLoading = false
        }
    }

    fun hideLoadMore() {
        mUserList.removeAt(mUserList.size - 1)
        val scrollPosition = mUserList.size
        mGridViewAdapter.notifyItemRemoved(scrollPosition)
    }
}