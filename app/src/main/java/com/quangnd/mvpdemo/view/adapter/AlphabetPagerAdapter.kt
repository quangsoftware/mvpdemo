package com.quangnd.mvpdemo.view.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.quangnd.mvpdemo.R
import kotlinx.android.synthetic.main.page_layout.view.*
import java.util.*

class AlphabetPagerAdapter(private val list: List<String>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container?.context)
                .inflate(R.layout.page_layout,container,false)

        // Set the text views text
        view.tv_lower.text = list.get(position)
        view.tv_upper.text = list.get(position).toUpperCase()
        view.tv_footer.text = "Page ${position+1} of ${list.size}"

        // Set the text views text color
        view.tv_lower.setTextColor(randomLightColor(70,80))
        view.tv_upper.setTextColor(randomLightColor(90,70))

        // Add the view to the parent
        container?.addView(view)

        // Return the view
        return view
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        // Remove the view from view group specified position
        parent.removeView(`object` as View)
    }

    // Generate random light hsv color
    private fun randomLightColor(lightPercent:Int,blackPercent:Int=100):Int{
        // Color variance - red, green, blue etc
        val hue = Random().nextInt(361).toFloat()

        // Color light to dark - 0 light 100 dark
        val saturation = lightPercent.toFloat()/100F

        // Color black to bright - 0 black 100 bright
        val brightness:Float = blackPercent.toFloat()/100F

        // Transparency level, full opaque
        val alpha = 255

        // Return the color
        return Color.HSVToColor(alpha,floatArrayOf(hue,saturation,brightness))
    }
}