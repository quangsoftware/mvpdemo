package com.quangnd.mvpdemo.view.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.quangnd.mvpdemo.R
import com.quangnd.mvpdemo.model.AlbumSDCard
import com.quangnd.mvpdemo.model.User
import com.quangnd.mvpdemo.utils.AppUtils
import com.quangnd.mvpdemo.view.adapter.PhotoAdapter
import com.quangnd.mvpdemo.view.adapter.UserAdapter
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_photo.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PhotoActivity : AppCompatActivity() {

    lateinit var listAlbum: ArrayList<AlbumSDCard>
    lateinit var listImage: ArrayList<String>
    lateinit var photoAdapter: PhotoAdapter;
    lateinit var dialog: AlertDialog
    var mCurrentPhotoPath: String = ""
    val CAMERA_IMAGE: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        listAlbum = ArrayList()
        listImage = ArrayList()


        photoAdapter = PhotoAdapter(listImage)
        rcvPhoto.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 4)
            adapter = photoAdapter
        }

        photoAdapter!!.setListener(object : PhotoAdapter.OnPhotoAction {
            override fun onClickItem(pathImage: String, position: Int) {
                if(position == 0){
                    if (ContextCompat.checkSelfPermission(this@PhotoActivity,
                                    Manifest.permission.CAMERA) !==
                            PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this@PhotoActivity,
                                        Manifest.permission.CAMERA)) {
                            ActivityCompat.requestPermissions(this@PhotoActivity,
                                    arrayOf(Manifest.permission.CAMERA), 1)
                        } else {
                            ActivityCompat.requestPermissions(this@PhotoActivity,
                                    arrayOf(Manifest.permission.CAMERA), 1)
                        }
                    } else {
                        openCamera()
                    }

                } else {
                    dialog = AppUtils.createPhotoAlertDialog(this@PhotoActivity, "Notification", listImage[position],"Yes", "No",
                        object : AppUtils.ActionAlert {
                            override fun onKeep() {

                                dialog?.dismiss()
                            }

                            override fun onDiscard() {
                                dialog?.dismiss()
                            }

                        })
                }
            }
        })

        getAlbumName()
        getAllShownImagesPath()
    }

    private fun openCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, createImageFile())
        val builder = StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        startActivityForResult(takePictureIntent, CAMERA_IMAGE)
    }

    private fun getAlbumName() {
        listAlbum.clear()
        val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
            MediaStore.Images.Media.DATA
        )
        val cursor = contentResolver.query(uri, projection, null, null, null)
        if (cursor != null) {
            var file: File? = null
            while (cursor.moveToNext()) {
                val bucketPath = cursor.getString(cursor.getColumnIndex(projection[0]))
                val fisrtImage = cursor.getString(cursor.getColumnIndex(projection[1]))
                val albumSDCard = AlbumSDCard()
                albumSDCard.name = bucketPath
                Log.d("PhotoActivity", albumSDCard.name)
                albumSDCard.path = fisrtImage
                file = File(fisrtImage)
                var isAdd = true
                if (file.exists()) {
                    for (i in 0 until listAlbum.size) {
                        if (listAlbum[i].path.contains(bucketPath)) {
                            isAdd = false
                            break
                        }
                    }
                    if (isAdd) {
                        listAlbum.add(albumSDCard)
                    }
                }
            }
            cursor.close()
        }
        val album = AlbumSDCard()
        album.name = "library"
        album.path = ""
        listAlbum.add(0, album)

    }

    private fun getAllShownImagesPath() {
        listImage.clear()
        val isSDPresent = android.os.Environment.getExternalStorageState() == android.os.Environment.MEDIA_MOUNTED
        if (isSDPresent) {
            val uri: Uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val cursor: Cursor?
            val column_index_data: Int
            var absolutePathOfImage: String? = null
            val projection = arrayOf(
                MediaStore.MediaColumns._ID,
                MediaStore.MediaColumns.DATA,
                MediaStore.MediaColumns.DATE_MODIFIED
            )
            cursor = MediaStore.Images.Media.query(
                contentResolver,
                uri,
                projection,
                null,
                MediaStore.MediaColumns.DATE_MODIFIED
            )
            column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data)
                listImage.add(absolutePathOfImage)
            }
            cursor.close()
            listImage.reverse()
        } else {
            val uri: Uri = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
            val cursor: Cursor?
            val column_index_data: Int
            var absolutePathOfImage: String? = null
            val projection = arrayOf(
                MediaStore.MediaColumns._ID,
                MediaStore.MediaColumns.DATA,
                MediaStore.MediaColumns.DATE_MODIFIED
            )
            cursor = MediaStore.Images.Media.query(
                contentResolver,
                uri,
                projection,
                null,
                MediaStore.MediaColumns.DATE_MODIFIED
            )
            column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data)
                listImage.add(absolutePathOfImage)
            }
            cursor.close()
            listImage.reverse()
        }
        listImage.add(0, "")
    }

    @Throws(IOException::class)
    private fun createImageFile(): Uri {
        var fileOutputStream: FileOutputStream? = null
        var bitmapFile: File? = null
        try {
            val file = File(Environment.getExternalStoragePublicDirectory("MVP"), "")
            if (!file.exists()) {
                file.mkdir()
            }
            val name: String = "IMG_" + SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().time) + ".jpg"
            bitmapFile = File(file, name)
            mCurrentPhotoPath = Environment.getExternalStorageDirectory().toString() + "/MVP/" + name
            fileOutputStream = FileOutputStream(bitmapFile)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush()
                    fileOutputStream.close()
                } catch (e: Exception) {
                }

            }
        }
        return Uri.fromFile(bitmapFile!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_IMAGE && resultCode == Activity.RESULT_OK) {
            if (mCurrentPhotoPath.isEmpty()) {
                return
            }
            dialog = AppUtils.createPhotoAlertDialog(this, "Notification", mCurrentPhotoPath,"Yes", "No",
                object : AppUtils.ActionAlert {
                    override fun onKeep() {

                        dialog?.dismiss()
                    }

                    override fun onDiscard() {
                        dialog?.dismiss()
                    }

                })
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.CAMERA) ===
                                    PackageManager.PERMISSION_GRANTED)) {
                        openCamera()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }
}