package com.quangnd.mvpdemo.view.base

import androidx.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BasePresenter<V>(@Volatile var view: V?) {

    companion object {
        var compositeDisposable: CompositeDisposable = CompositeDisposable()
    }

    init {

    }

    protected fun view(): V? {
        return view
    }

    @CallSuper
    fun unbindView() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
        this.view = null
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
}