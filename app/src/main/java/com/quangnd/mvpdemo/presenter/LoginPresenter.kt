package com.quangnd.mvpdemo.presenter

interface LoginPresenter {
    fun login(email: String, password: String)
}