package com.quangnd.mvpdemo.presenter

import com.quangnd.mvpdemo.model.User

interface UserView {
    fun getListUserFail(error: String)
    fun getListUserSuccess(userList: List<User>, page: Int)
}