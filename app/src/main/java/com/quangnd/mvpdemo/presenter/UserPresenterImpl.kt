package com.quangnd.mvpdemo.presenter

import android.app.Activity
import com.google.gson.JsonObject
import com.quangnd.mvpdemo.data.api.ServiceBuilder
import com.quangnd.mvpdemo.model.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class UserPresenterImpl(var action: UserView, var mActivity: Activity) : UserPresenter {

    private val sharedPrefFile = "kotlinsharedpreference"

    override fun getListUser(page: Int) {
            val compositeDisposable = CompositeDisposable()
            compositeDisposable.add(
                ServiceBuilder.buildService().getListUser(page)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ response -> onResponse(response) }, { t -> onFailure(t) })
            )

    }

    private fun onFailure(t: Throwable) {
        action.getListUserFail(t.message.toString())
    }

    private fun onResponse(response: JsonObject) {
        val data = JSONObject(response.toString())
        val userList = ArrayList<User>()
        val userJsonArr  = data.getJSONArray("data")
        for (i in 0 until  userJsonArr.length()) {
            userList.add(User.parseData(userJsonArr.getJSONObject(i)))
        }

        val nextPage = data.optInt("page") + 1
        action.getListUserSuccess(userList, nextPage)
    }

}