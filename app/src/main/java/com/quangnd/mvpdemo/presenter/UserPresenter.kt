package com.quangnd.mvpdemo.presenter

interface UserPresenter {
    fun getListUser(page: Int)
}