package com.quangnd.mvpdemo.presenter

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import android.util.Log
import com.quangnd.mvpdemo.data.api.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class LoginPresenterImpl(var action: LoginView, var mActivity: Activity) : LoginPresenter {

    private val sharedPrefFile = "kotlinsharedpreference"

    override fun login(email: String, password: String) {
        if (TextUtils.isEmpty(email)) {
            action.validateFail("Enter is email")

        } else if (TextUtils.isEmpty(password)) {
            action.validateFail("Enter is password")

        } else {
            val compositeDisposable = CompositeDisposable()
            compositeDisposable.add(
                ServiceBuilder.buildService().login(email, password)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ response -> onResponse(response) }, { t -> onFailure(t) })
            )
        }
    }

    private fun onFailure(t: Throwable) {
        action.loginFail(t.message.toString())
    }

    private fun onResponse(response: JSONObject) {
        Log.d("LoginPresenterImpl", "${response.toString()}")

        val sharedPreferences: SharedPreferences = mActivity.getSharedPreferences(sharedPrefFile,
            Context.MODE_PRIVATE)
        val editor:SharedPreferences.Editor =  sharedPreferences.edit()

        val token = response.optString("token")

        editor.putString("token",token)
        editor.apply()
        editor.commit()

        action.loginSuccess()
    }

}