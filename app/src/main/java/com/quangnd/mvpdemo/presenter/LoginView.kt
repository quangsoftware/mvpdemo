package com.quangnd.mvpdemo.presenter

interface LoginView {
    fun loginSuccess()
    fun loginFail(error: String)
    fun validateFail(error: String)
}